<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language ?>" xml:lang="<?php print $language ?>">
<head>
<title><?php print $head_title ?></title>
<meta http-equiv="Content-Style-Type" content="text/css" />
<?php print $head ?><?php print $styles ?>
</head>
<body <?php print theme("onload_attribute"); ?>> 

<div id="container">
<div id="skip">
	<p><a href="#content" title="Skip to site content">Skip to content</a></p>
	<p><a href="#search" title="Skip to search" accesskey="s">Skip to search - Accesskey = s</a></p>
</div>
<hr />

<h1><a href="<?php print url() ?>" title="<?php print($site_name) ?>"><?php print($site_name) ?></a></h1> 
<div class="tagline"><?php print($site_slogan) ?></div> 

	<div id="content_bg">
	<!-- Needed for dropshadows -->
	<div class="container_left">
	<div class="container_right">
	<div class="topline">
	<!-- Start float clearing -->
	<div class="clearfix">
	<!-- end header -->
<!-- begin sidebar -->
<div id="right">
	<?php if (is_array($primary_links)) : ?>
	<div class="block block-nav">
	<h2>Navigation</h2>
	<div class="content"><div class="item-list">
	<ul> 
	<?php foreach ($primary_links as $link): ?> 
	<li><?php print $link?></li> 
	<?php endforeach; ?> 
	</ul></div></div>
	</div><div class="line"></div>
	<?php endif; ?>
	<?php if ($search_box): ?>
	<h2>Search</h2>
	<p class="searchinfo">search site archives</p>
	<div id="search">
	<div id="search_area"> 
	<form action="<?php print $search_url ?>" method="post" id="searchform"> 
	<input class="form-text" type="text" size="15" value="" name="edit[keys]" id="s" /> 
	<input class="form-submit" type="submit" value="<?php print $search_button_text ?>" id="searchsubmit" /> 
	</form> 
	</div>
	</div>
	<?php endif; ?> 
	<?php print $sidebar_left ?>
	<?php print $sidebar_right ?>
</div>
<hr />

<div id="content"> 
<div class="navigation"> <?php print $breadcrumb ?> </div> 
<?php if ($messages != ""): ?> 
<div id="message"><?php print $messages ?></div> 
<?php endif; ?> 
<?php if ($mission != ""): ?> 
<div id="mission"><?php print $mission ?></div> 
<?php endif; ?> 
<?php if ($title != ""): ?> 
<h2 class="page-title"><?php print $title ?></h2> 
<?php endif; ?> 
<?php if ($tabs != ""): ?> 
<?php print $tabs ?> 
<?php endif; ?> 
<?php if ($help != ""): ?> 
<p id="help"><?php print $help ?></p> 
<?php endif; ?> 
<!-- start main content --> 
<?php print($content) ?> 
<!-- end main content --> 
</div> 

<!-- End float clearing -->
</div>
<!-- End content -->
<!-- begin footer -->
<hr />

<div id="footer"> 
<p> 
<?php if ($footer_message) : ?> 
<?php print $footer_message;?><br /> 
<?php endif; ?> 
<p><?php print(variable_get('site_name', 'drupal')) ?> is proudly powered by <a href="http://drupal.org">Drupal</a>. The Green Marin&eacute;e template by <a href="http://e-lusion.com" title="Ian Main - e-lusion.com">Ian Main</a> - Built for <a href="http://wordpress.org" title="Wordpress - As lovely as your Mum!">Wordpress 1.5</a>, ported to Drupal by <a href="http://goodbasic.com/">GoodBasic</a>.</p> 
</div>
<!-- End Footer -->

</div>
</div>
</div>
</div>
</div>
<?php print $closure;?> 
</body>
</html>