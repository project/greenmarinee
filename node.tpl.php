<div class="<?php print ($sticky) ? " sticky" : ""; ?>"> 
  <?php if ($page == 0): ?> 
  <h2><a href="<?php print $node_url ?>" rel="bookmark" title="Permanent Link to <?php print $title ?>"><?php print $title ?></a></h2> 
  <?php endif; ?> 
  <div class="meta">Posted in <?php print $terms ?> by <?php print $name ?> on <?php print $date ?></div> 
  <div class="main"> <?php print $content ?> </div> 
  <?php if ($links): ?> 
  <div class="comments"><?php print $links ?></p> 
  <?php endif; ?> 
</div>